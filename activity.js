{	
	{
		"first name": "Juan",
		"last name": "Dela Cruz",
		"email": "j.delacruz@gmail.com",
		"password": "jdc456",
		"is admin?": "false",
		"mobile number": "0912-345-6789"
	}
	{
		"user id": "user123",
		"transaction date": "March 27, 2023",
		"status": "paid",
		"total": "3"
	}
	{
		"name": "AOC 24G2E",
		"description": "AOC 24 inch 1080p 144hz monitor",
		"price": "₱10395",
		"stocks": "4",
		"is active?": "true",
		"SKU": "XYZ12345"
	}
	{
		"order id": "order123",
		"product id": "product123",
		"quantity": "2",
		"price": "₱21790",
		"sub total": "₱21790"
	}
}